#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <signal.h>

pid_t child1Pid = 0;
pid_t child2Pid = 0;
static int loop = 1;

int calcHalfLength(int length);
void sigHandler(int sig);

int main(int argc, char* argv[])
{
    if (argc < 2)
    {
        fprintf(stderr, "Brak argumentow programu!\n");
        exit(EXIT_FAILURE);
    }

    sigset_t sig;
    sigemptyset(&sig);
    sigaddset(&sig, SIGTSTP);
    sigprocmask(SIG_BLOCK, &sig, NULL);

    struct sigaction sa;
    sigemptyset(&sa.sa_mask);
    sa.sa_handler = &sigHandler;
    sigaction(SIGINT, &sa, NULL);

    char* arg = argv[argc - 1];
    size_t length = strlen(arg);
    int halfLength = calcHalfLength((int)length);

    char* firstHalf = (char*)malloc((halfLength + 1) * sizeof(char*));
    char* secondHalf = (char*)malloc((halfLength + 1) * sizeof(char*));
    strncpy(firstHalf, arg, halfLength);
    firstHalf[halfLength] = '\0';
    strncpy(secondHalf, arg + halfLength, halfLength);
    secondHalf[halfLength] = '\0';

    char** args = (char**)malloc((argc + 2) * sizeof(char*));
    for (int i = 0; i < argc; i++)
    {
        args[i] = (char*)malloc(strlen(argv[i]) + 1);
        strcpy(args[i], argv[i]);
    }
    args[argc + 1] = NULL;

    if (length > 1)
    {
        child1Pid = fork();
        if (child1Pid == 0)
        {
            setpgid(0, 0);
            args[argc] = firstHalf;
            execv(argv[0], args);
        }
        else if (child1Pid > 0)
        {
            child2Pid = fork();
            if (child2Pid == 0)
            {
                setpgid(0, 0);
                args[argc] = secondHalf;
                execv(argv[0], args);
            }
        }
    }

    while (loop);

    if (child1Pid > 0 || child2Pid > 0)
        while (wait(NULL) > 0);

    printf("%d ", getpid());
    for (int i = 1; i < argc; i++)
    {
        printf("%s ", argv[i]);
        free(args[i]);
    }
    printf("\n");

    free(args);
    free(firstHalf);
    free(secondHalf);

    sigset_t stpSig;
    sigemptyset(&stpSig);
    sigpending(&stpSig);
    if (sigismember(&stpSig, SIGTSTP) == 1)
        printf("Odebrano sygnal SIGSTP\n");

    sigprocmask(SIG_UNBLOCK, &sig, NULL);

    return EXIT_SUCCESS;
}

int calcHalfLength(int length)
{
    int powerOf2 = 1;
    while (length >>= 1)
    {
        powerOf2 <<= 1;
    }
    return powerOf2 / 2;
}

void sigHandler(int sig)
{
    if (child1Pid > 0)
        kill(child1Pid, SIGINT);

    if (child2Pid > 0)
        kill(child2Pid, SIGINT);

    loop = 0;
}
